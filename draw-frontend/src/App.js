import React, {Component} from 'react';


class App extends Component {
  componentDidMount() {
      this.websocket = new WebSocket('ws://localhost:8000/paint');

      this.websocket.onmessage = (pixels) => {
          const decodedMessage = JSON.parse(pixels.data);

          switch (decodedMessage.type) {
              case 'NEW_CANVAS':
                  this.context = this.canvas.getContext('2d');
                  this.imageData = this.context.createImageData(1, 1);
                  this.d = this.imageData.data;

                  this.d[0] = 0;
                  this.d[1] = 0;
                  this.d[2] = 0;
                  this.d[3] = 255;
                  decodedMessage.pixels.forEach(pixel => {
                      this.context.putImageData(this.imageData, pixel.x, pixel.y);
                  });

                  break;
          }
      };

  }

  state = {
    mouseDown: false,
    pixelsArray: [],
    pixels: ""
  };

  canvasMouseMoveHandler = event => {
    if (this.state.mouseDown) {
      event.persist();
      this.setState(prevState => {
        return {
          pixelsArray: [...prevState.pixelsArray, {
            x: event.clientX,
            y: event.clientY
          }]
        };
      });

      this.context = this.canvas.getContext('2d');
      this.imageData = this.context.createImageData(1, 1);
      this.d = this.imageData.data;

      this.d[0] = 0;
      this.d[1] = 0;
      this.d[2] = 0;
      this.d[3] = 255;

      this.context.putImageData(this.imageData, event.clientX, event.clientY);

    }
  };

  mouseDownHandler = event => {
    this.setState({mouseDown: true});
  };


  mouseUpHandler = async event => {
    const paint = JSON.stringify({
        type: 'SEND_PAINT',
        text: this.state.pixelsArray
    });
      await this.websocket.send(paint);
      this.setState({mouseDown: false, pixelsArray: []});
  };

  render() {
    return (
      <div>
        <canvas
          ref={elem => this.canvas = elem}
          style={{border: '1px solid black'}}
          width={800}
          height={600}
          onMouseDown={this.mouseDownHandler}
          onMouseUp={this.mouseUpHandler}
          onMouseMove={this.canvasMouseMoveHandler}
        />
      </div>
    );
  }
}

export default App;
